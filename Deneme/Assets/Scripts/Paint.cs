﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paint : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerMovement.instance.SetCanMoveWithPaintTrigger(10, false, true);
            UIConroller.instance.finishLine = false;
            UIConroller.instance.quitButton.SetActive(true);
            UIConroller.instance.restartButton.SetActive(true);
            UIConroller.instance.paintMe.SetActive(true);
        }
    }
}
