﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class donutrotate : MonoBehaviour
{
    public float rotspeed = 10.0f;
    private bool _qCoroutine = true;
    private float _time = 4;

   /* void Start()
    {
        yield return new WaitForSeconds(5);

    }*/
    private void FixedUpdate()
    {
        if (_qCoroutine)
        {
            if (_time > 0)
            {
                transform.Rotate(Vector3.left, Time.fixedDeltaTime * rotspeed);
                _time -= Time.fixedDeltaTime;
            }
            else
            {
                StartCoroutine(RotateForSeconds());
                _qCoroutine = false;
            }
        }
       
    }
    IEnumerator RotateForSeconds()
    {
        yield return new WaitForSeconds(2);
        _time = 10;
        _qCoroutine = true;

    }

}
