﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotating : MonoBehaviour
{
    public Rigidbody rb;
    public float force;
    public float rot;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        transform.Rotate(new Vector3(0, 0, 90) * Time.fixedDeltaTime * rot);

    }
    private void OnCollisionStay(Collision collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Rigidbody>().AddRelativeForce(-Vector3.right * force);
        }
        else
        {
            collision.gameObject.GetComponent<Rigidbody>().AddRelativeForce(-Vector3.right * force / 2);
        }

    }
}