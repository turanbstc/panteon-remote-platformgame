﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToTarget : MonoBehaviour
{
    public int id;
    public Transform target;
    public Vector3 _objPosition;
    public UnityEngine.AI.NavMeshAgent agent;

    private Animator anim;

    [SerializeField]
    float destinationReachedTreshold;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        _objPosition = transform.position;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.SetDestination(new Vector3(transform.position.x, transform.position.y, target.position.z));
        anim = GetComponent<Animator>();
        StartCoroutine(ReportDistance());
    }
    private void Update()
    {
        CheckDestinationReached();
    }


    void CheckDestinationReached()
    {
        if (target.position.z-transform.position.z<1)
        {
            agent.SetDestination(transform.position);
            anim.SetBool("GirlDance", true);
           
        }
         
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle") || collision.gameObject.CompareTag("Spawn"))
        {

            transform.position = _objPosition;

        }
    }
    
    IEnumerator ReportDistance()
    {
        yield return new WaitForSeconds(0.5f);
        float distance = target.position.z - transform.position.z;
        UIConroller.instance.RankPosition(distance, id);
        StartCoroutine(ReportDistance());
        
    }

    

}



