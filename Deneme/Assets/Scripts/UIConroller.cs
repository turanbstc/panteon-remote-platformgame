﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIConroller : MonoBehaviour
{
    public GameObject playButton;
    public GameObject restartButton;
    public GameObject quitButton;
    public Text currentRank;
    public GameObject paintMe;
    public bool finishLine = true;
    public float[] rank;

    private int _playerRank=11;
    private float _playerDistance;    


    public static UIConroller instance;
    void Start()
    {
        Time.timeScale = 0;
        instance = this;
        StartCoroutine(Call());
        paintMe.SetActive(false);
        restartButton.SetActive(false);

    }

    public void OnClikPlayButton()
    {
        Time.timeScale = 1;
        playButton.SetActive(false);
        
        quitButton.SetActive(false);
    }
    
    public void OnClickRestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }

    public void OnClickQuitButton()
    {
        Application.Quit();
    }

    public void RankPosition(float i, int index)
    {
        rank[index] = i;
    }
    IEnumerator Call()
    {
        yield return new WaitForSeconds(0.5f);
        _playerDistance = rank[10];
        
        Array.Sort(rank);
        if (rank[10] > 0) 
        {
            for (int i = 0; i < rank.Length; i++)
            {
                if (rank[i] == _playerDistance)
                {
                    _playerRank = i+1;
                    break;
                }
            }
            currentRank.text = "Current Rank: " + _playerRank.ToString();
        }
        if (finishLine)
        { 
              StartCoroutine(Call());
        }
        
    }

}
