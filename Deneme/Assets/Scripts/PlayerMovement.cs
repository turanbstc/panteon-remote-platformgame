﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    private Touch _touch;
    private float _speed = 5f;
    private Vector2 _startPosition;
    private Vector2 _sense;
    private Vector2 _normdirection;
    private Vector3 _playerPosition;
    private bool _rotatingplatform = false;
    private bool _move = false;//checks if there is input
    private bool _doMove = true;//checks if player fall.
   
    public Rigidbody rb;
    public int id;
    public Transform target;
    public Animator anim;
    public GameObject painter;   


    public static PlayerMovement instance;
    
    void Start()
    {
        instance = this;
        _playerPosition = transform.position;
        StartCoroutine(ReportDistance());
      
    }
    

    void Update()
    {
#if UNITY_ANDROID //please remove comment lines if you would like to play on mobile (touch)
       /* if(_canmove)
        {
        _touch = Input.GetTouch(0);

        if (_touch.phase == TouchPhase.Began)
        {
            _startPos = _touch.position;
                _move = true;
        }

        if (_touch.phase == TouchPhase.Moved)
        {
            rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            _direction = _touch.position - _startPos;
            _normdirection = _direction.normalized;
            Move(_normdirection);
        }

        if (_touch.phase == TouchPhase.Ended)
        {
            StopMovement();
        }

        }*/
        
#endif


#if UNITY_EDITOR
        if (_doMove)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _startPosition = Input.mousePosition;
                _move = true;
            }

            if (Input.GetMouseButton(0))
            {
               
                Vector2 currentpos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                _sense = currentpos - _startPosition;
            }

            if (Input.GetMouseButtonUp(0))
            {
                StopMovement();
            }
        }
        
#endif
    }

    private void FixedUpdate()
    {
        if (_sense.magnitude >= 5 && _move)
        {
            _normdirection = _sense.normalized;
            Move(_normdirection);
        }
    }

    void Move(Vector2 direct)
    {
        Vector3 rotpos = transform.position - new Vector3(direct.x + transform.position.x, transform.position.y, direct.y + transform.position.z);
        anim.SetBool("BoyRun", true);
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        Quaternion rotation = Quaternion.LookRotation(-1 * rotpos);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10 * Time.fixedDeltaTime);

        if (!_rotatingplatform)
        {
            var vec3 = transform.forward * _speed;
            vec3.y = rb.velocity.y;
            rb.velocity = vec3;
        }
        else
        {
            var nvelocity = transform.forward * _speed;
            nvelocity.x += -transform.right.x * 5;
            nvelocity.y = rb.velocity.y;
            rb.velocity = nvelocity;
        }
    }

    void StopMovement()
    {
        _move = false;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        anim.SetBool("BoyRun", false);
    }

    public void SetCanMoveWithFall(bool canmove)
    {
        _doMove = canmove;
        if (!_doMove)
        {
            anim.SetTrigger("Fall");
            anim.SetBool("Idle", false);
            StopMovement();
        }

        StartCoroutine(WaitForAnim());
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
           SetCanMoveWithFall(false);
        }
        
       
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Opponent"))
        {
            rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Opponent"))
        {
            rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }
    }

    public void SetCanMoveWithPaintTrigger(int camyaxisvalue, bool canmove, bool ispainting)
    {
        CameraController.instance.offset = new Vector3(CameraController.instance.offset.x, camyaxisvalue, CameraController.instance.offset.z);
        painter.SetActive(ispainting);
        _doMove = canmove;
       
        if (!_doMove)
        {
            StopMovement();
        }
        
    }

    IEnumerator WaitForAnim()
    {
        yield return new WaitForSeconds(1);
        _doMove = true;
        anim.SetBool("Idle",true);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Spawn"))
        {
            transform.position = _playerPosition;
        }
    }
    IEnumerator WaitForStart()
    {
        yield return new WaitForSeconds(2);
    }
    IEnumerator ReportDistance()
    {

        yield return new WaitForSeconds(0.5f);
        float dist = target.position.z - transform.position.z;
        UIConroller.instance.RankPosition(dist, id);
        StartCoroutine(ReportDistance());
        
    }
}
