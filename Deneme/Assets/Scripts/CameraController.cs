﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public Transform target;
	public float transitionSpeed = 0.1f;
	public Vector3 offset;


	public static CameraController instance;
    private void Start()
    {
		instance = this;
    }

    void FixedUpdate()
	{
		Vector3 intendedPosition = target.position + offset;
		Vector3 smoothedPosition = Vector3.Lerp(transform.position, intendedPosition, transitionSpeed);
		transform.position = smoothedPosition;

		transform.LookAt(target);
	}
}
