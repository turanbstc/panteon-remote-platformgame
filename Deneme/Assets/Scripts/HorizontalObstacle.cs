﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalObstacle : MonoBehaviour
{
    private Vector3 positionDisplacement;
    private Vector3 positionOrigin;
    private float _timePassed;
    [SerializeField]
    private bool _newDirection;
    private void Start()
    {
        if (_newDirection)
        {
            positionDisplacement = new Vector3(26.7f, transform.position.y, transform.position.z);
            positionOrigin = new Vector3(12.64f, transform.position.y, transform.position.z);
        }
        else
        {
            
            positionDisplacement = new Vector3(12.64f, transform.position.y, transform.position.z);
            positionOrigin = new Vector3(26.7f, transform.position.y, transform.position.z);
        }
    }

    private void Update()
    {
        _timePassed += Time.deltaTime / 2;
        transform.position = Vector3.Lerp(positionOrigin, positionDisplacement,
            Mathf.PingPong(_timePassed, 1));

    }
    
}
